"""This module contains functions for converting values from one type to other."""


import uuid
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from .enums import Access, Priority, Status, Days


def empty_to_none(str_):
    """Returns None value if str_ is empty, else returns str_."""
    if str_ == "" or str_ == "нет" or str_ == "no":
        return None
    else:
        return str_


def to_date(str_):
    """
    Convert str_ to datetime.date object.

    Args:
        str_: String to convert.

    Returns:
        Returns None if str_ is empty or None, else datetime.date object.

    Raises:
        ValueError if str_ is not a valid date string.
    """
    if str_ is None or str_ == "":
        return None
    elif isinstance(str_, date):
        return str_
    else:
        try:
            return datetime.strptime(str_, "%d %m %Y").date()
        except:
            raise ValueError("Неверный формат даты - '{}'.".format(str_))


def to_time(str_):
    """
    Converts str_ to datetime.time object.

    Args:
        str_: String to convert.

    Returns:
        Returns None if str_ is empty or None, else datetime.time object.

    Raises:
        ValueError if str_ is not a valid date string.
    """
    if str_ == "" or str_ is None:
        return None
    else:
        try:
            return datetime.strptime(str_, "%H:%M").time()
        except:
            raise ValueError("Неверный формат времени - '{}'.".format(str_))


def to_uuid(str_):
    """Converts str_ string to uuid."""
    if str_ is None:
        return None
    else:
        return uuid.UUID(str_)


def date_to_str(date):
    """Converts datetime.date to str format '%d.%m.%Y'."""
    if date is None:
        return date
    elif isinstance(date, str):
        return date
    else:
        return datetime.strftime(date, "%d.%m.%Y")


def time_to_str(time):
    """Converts datetime.time to str format '%H:%M'."""
    return time.strftime("%H:%M")


def date_time_to_str(date_time):
    """Convert datetime to str format '%d.%m.%Y %H:%M'"""
    date_ = date_time.date()
    time_ = date_time.time()
    return date_to_str(date_) + " " + time_to_str(time_)


def get_color(priority):
    """Returns special color for priority value."""
    if priority == 0:
        return "white"
    elif priority == 1:
        return "green"
    elif priority == 2:
        return "yellow"
    elif priority == 3:
        return "red"
    else:
        raise ValueError("Передано неверное значение параметра priority '{}'".format(priority))


def str_to_priority(str_):
    """Converts str_ to Priority object."""
    if str_ == "":
        return Priority.NO
    if str_ == '0':
        return Priority.NO
    elif str_ == '1':
        return Priority.LOW
    elif str_ == '2':
        return Priority.MEDIUM
    elif str_ == '3':
        return Priority.HIGH
    else:
        raise ValueError("Передано неверное значение параметра - '{}'".format(str_))


def int_to_priority(int_):
    """Converts int to Priority object."""
    if int_ == 0:
        return Priority.NO
    elif int_ == 1:
        return Priority.LOW
    elif int_ == 2:
        return Priority.MEDIUM
    elif int_ == 3:
        return Priority.HIGH
    else:
        raise ValueError("Передано неверное значение параметра - '{}'".format(int_))


def priority_to_str(priority):
    """Returns string that contains priority name."""
    if not isinstance(priority, Priority):
        raise ValueError
    if priority == Priority.LOW:
        return "низкий"
    elif priority == Priority.MEDIUM:
        return "средний"
    elif priority == Priority.HIGH:
        return "высокий"
    else:
        return "нет"


def str_to_access(str_):
    """Converts str_ to Access object."""
    if str_ == "":
        return Access.PRIVATE
    if str_ == 'p':
        return Access.PRIVATE
    elif str_ == 'r':
        return Access.READE
    elif str_ == 'w':
        return Access.WRITE
    else:
        raise ValueError


def str_to_status(str_):
    """Converts str_ to Status object."""
    if str_ == 'active':
        return Status.ACTIVE
    elif str_ == 'ready':
        return Status.READY
    elif str_ == 'failed':
        return Status.FAILED
    elif str_ == 'deleted':
        return Status.deleted
    else:
        raise ValueError("Передано неверное значение параметра - '{}'".format(str_))


def str_to_display(str_):
    """Returns 'нет' if str_ is empty or None, else returns str_."""
    if str_ == "" or str_ is None:
        return "нет"
    return str_


def parse_group(str_):
    """Returns list of group names."""
    return str_.translate(str.maketrans(',', " ")).split()


def parse_days(str_):
    """Returns dict in which day number is key and Days object is value."""

    days = str_.translate(str.maketrans(',', " ")).split()
    return {(int(item)): Days(int(item)) for item in days}


def rel_to_str(rel):
    """Converts relativedelta object to str format 'days_amount mounts_amount years_amount'."""
    days = str(rel.days)
    month = str(rel.months)
    years = str(rel.years)
    return days + " " + month + " " + years


def str_to_rel(str_):
    """
    Converts str_ to relativedelta object.

    Args:
        str_: String to convert.
    Returns:
        Relativedelta object.
    Raises:
        ValueError if str_ format is not 'days_amount mounts_amount years_amount'."""
    str_ = str_.split()
    if str_[0] != '0':
        return relativedelta(days=int(str_[0]))
    elif str_[1] != '0':
        return relativedelta(months=int(str_[1]))
    else:
        return relativedelta(years=int(str_[2]))


def rel_to_display(rel):
    """Return string that implements relativedelta object."""
    if rel.days != 0:
        return str(rel.days) + " (в днях)"
    elif rel.months != 0:
        return str(rel.months) + " (в месяцах)"
    else:
        return str(rel.years) + " (в годах)"


def plan_days_to_display(days):
    """Returns str that contains days names."""
    days = parse_days(days)
    result_str = ""
    for value in days.values():
        result_str += str(value) + ", "
    return result_str[:-2]
