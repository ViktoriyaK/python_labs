"""
This library implements classes and functions for Tracker application.

Lib modules contains User, Task, Event, EventPlan and EventPlanDays classes that are models and allows create,
edit, delete tasks, events, create event plans that allows add an event once and repeat it according to the
specified conditions.

Modules:
    *db.py - Contains Task, Event, EventPlan, EventPlanDays and User models that implements tables in database for
     work with Peewee ORM, Adapter class that establishes connection with database, creating and dropping tables.

    *storage.py - Contains TaskStorage, EventStorage, PlanStorage, UserStorage and GroupStorage classes that
     allows managing tasks, events, event plans in database.

    *enums.py - Contains Priority, Access, Status and Days enums.

    *task.py - Contains Task and Subtask classes.

    *event.py - Contains Event class.

    *plan.py - Contains EventPlan and EventPlanDays classes.

    *user.py - Contains User class.

    *tools.py - Contains functions for converting values from one type to other.
"""