"""This module contains Event class."""


import datetime
from .enums import Access, Priority


class Event:
    """
    Class for storing users events.

    Events differ from tasks in that they must have a date and time field and don't have status field.
    """
    def __init__(
            self,
            title,
            date,
            time,
            create_date=datetime.datetime.now().date(),
            priority=None,
            group=None,
            description=None,
            id=None,
            access=Access.PRIVATE,
            plan_id=None,
            edit_date=datetime.datetime.now()
    ):
        """Initializes event fields."""
        if priority is None:
            priority = Priority.NO

        self.id = id
        self.title = title
        self.create_date = create_date
        self.date = date
        self.time = time
        self.priority = priority
        self.description = description
        self.group = group
        self.access = access
        self.plan_id = plan_id
        self.edit_date = edit_date


    @staticmethod
    def sort_by_time(event):
        return event.time
