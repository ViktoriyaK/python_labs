"""This module contains User class."""


class User:
    """Class for storing user information."""
    def __init__(self, name, login, email, id=None):
        self.id = id
        self.name = name
        self.login = login
        self.email = email
