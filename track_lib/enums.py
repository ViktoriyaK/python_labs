"""This module contains enums classes."""


from enum import IntEnum, Enum


class Priority(IntEnum):
    """Enum for store task priority."""
    NO = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3


class Access(Enum):
    """Enum for store access level for tasks and events."""
    PRIVATE = 'p'
    READE = 'r'
    WRITE = 'w'

    def __str__(self):
        if self == Access.PRIVATE:
            return "приватный"
        elif self == Access.READE:
            return "чтение"
        elif self == Access.WRITE:
            return "чтение и редактирование"


class Status(Enum):
    """Enum for store task status."""
    ACTIVE = 'active'
    READY = 'ready'
    FAILED = 'failed'

    def __str__(self):
        if self == Status.ACTIVE:
            return "активная"
        elif self == Status.READY:
            return "завершенная"
        elif self == Status.FAILED:
            return "проваленная"


class Days(Enum):
    """This enum contains week days."""
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    def __str__(self):
        if self.value == 0:
            return "понедельник"
        elif self.value == 1:
            return "вторник"
        elif self.value == 2:
            return "среда"
        elif self.value == 3:
            return "четверг"
        elif self.value == 4:
            return "пятница"
        elif self.value == 5:
            return "суббота"
        elif self.value == 6:
            return "воскресенье"
