"""
This module contains Task, Event, EventPlan, EventPlanDays and User models that implements tables in database,
Adapter class that establishes connection with database, creating and dropping tables, also contains DeltaDateField,
PriorityField, AccessField and StatusField that are custom field types.
"""


from peewee import (
    Proxy,
    Model,
    PrimaryKeyField,
    ForeignKeyField,
    DateTimeField,
    DateField,
    TimeField,
    TextField,
    UUIDField,
    Field,
    SqliteDatabase
)
from track_lib.tools import (
    rel_to_str,
    str_to_rel,
    int_to_priority,
    str_to_access,
    str_to_status
)


database_proxy = Proxy()


class DeltaDateField(Field):
    """Custom field for relativedelta type."""
    field_type = 'relativedelta'

    def db_value(self, value):
        return rel_to_str(value)

    def python_value(self, value):
        return str_to_rel(value)


class PriorityField(Field):
    """Custom field for Priority type."""
    field_type = 'Priority'

    def db_value(self, value):
        return str(value.value)

    def python_value(self, value):
        return int_to_priority(value)


class AccessField(Field):
    """Custom field for Access type."""
    field_type = 'Access'

    def db_value(self, value):
        return value.value

    def python_value(self, value):
        return str_to_access(value)


class StatusField(Field):
    """Custom field for Status type."""
    field_type = 'Status'

    def db_value(self, value):
        return value.value

    def python_value(self, value):
        return str_to_status(value)


class BaseModel(Model):
    """Base class for classes that work with Peewee."""
    class Meta:
        database = database_proxy


class User(BaseModel):
    """Implements User table."""
    user_id = PrimaryKeyField(null=False)
    name = TextField()
    login = TextField(unique=True)
    email = TextField()


class Event(BaseModel):
    """Implements Event table."""
    event_id = PrimaryKeyField(null=False)
    title = TextField(null=False)
    create_date = DateField(null=False)
    date = DateField(null=False)
    time = TimeField(null=False)
    priority = PriorityField(null=False)
    group = TextField(null=True)
    description = TextField(null=True)
    user = ForeignKeyField(User, backref='events', null=False)
    access = AccessField(null=False)
    event_plan_id = UUIDField(null=True)
    edit_date = DateTimeField(null=False)


class EventPlan(BaseModel):
    """Implements EventPlan table."""
    event_plan_id = UUIDField(null=False)
    event = ForeignKeyField(Event, backref='event_plan', null=False)
    delta_date = DeltaDateField(null=False)
    first_date = DateField(null=False)
    user = ForeignKeyField(User, backref='event_pans', null=False)
    edit_date = DateTimeField(null=False)


class EventPlanDays(BaseModel):
    """Implements EventPlanDays table."""
    event_plan_days_id = UUIDField(null=False)
    event = ForeignKeyField(Event, backref='event_plan_days', null=False)
    days = TextField(null=False)
    first_date = DateField(null=False)
    user = ForeignKeyField(User, backref='event_pans_days', null=False)
    edit_date = DateTimeField(null=False)


class Task(BaseModel):
    """Implements Task table."""
    task_id = PrimaryKeyField(null=False)
    title = TextField(null=False)
    create_date = DateField(null=False)
    deadline_date = DateField(null=True)
    priority = PriorityField(null=False)
    group = TextField(null=True)
    description = TextField(null=True)
    access = AccessField(null=False)
    status = StatusField(null=False)
    related_tasks = TextField(null=True)
    parent_task = ForeignKeyField('self', related_name='subtasks', null=True)
    user = ForeignKeyField(User, backref='tasks', null=False)
    edit_date = DateTimeField(null=False)


class Adapter:
    """Class for establishing connection with database, creating and dropping tables."""

    def __init__(self, database_name):
        self.database_name = database_name
        self.database = SqliteDatabase(database_name)
        self.connected = False
        database_proxy.initialize(self.database)
        self.create_tables()

    def create_tables(self):
        if not self.connected:
            self.database.connect()
            self.connected = True

        User.create_table(True)
        Event.create_table(True)
        Task.create_table(True)
        EventPlan.create_table(True)
        EventPlanDays.create_table(True)

    def drop_tables(self):
        self.database.drop_tables(
            [Task, User, Event, EventPlan, EventPlanDays])
        self.database.close()
        self.connected = False
