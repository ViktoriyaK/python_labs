"""This module allows managing tasks, events, event plans in database."""


from track_lib import db
from track_lib import tools
from datetime import datetime
from track_lib.enums import Status
from track_lib.task import Task, Subtask
from track_lib.event import Event
from track_lib.plan import EventPlan, EventPlanDays


class TaskStorage:
    """
    This class contains static methods that allows save, update, delete tasks in database,
    returns tasks by specified parameters from database.
    """

    @staticmethod
    def save_task(task, user_id, parent_task_id=None):
        """Save user task in database."""
        db.Task.create(
            title=task.title,
            create_date=task.create_date,
            deadline_date=task.deadline_date,
            priority=task.priority,
            group=task.group,
            description=task.description,
            access=task.access,
            status=task.status,
            related_tasks=task.related_tasks,
            parent_task_id=parent_task_id,
            user_id=user_id,
            edit_date=task.edit_date
        )

    @staticmethod
    def get_user_tasks_by_date(date, user_id, status):
        """Returns user tasks by date and status."""
        return TaskStorage._init_task(
            db.Task.select().where(
                (db.Task.deadline_date == date) &
                (db.Task.user_id == user_id) &
                (db.Task.parent_task_id.is_null(True)) &
                (db.Task.status == status)
            )
        )

    @staticmethod
    def get_user_tasks_by_title(title, user_id, status=Status.ACTIVE):
        """Returns user tasks by title and status."""
        return TaskStorage._init_task(
            db.Task.select().where(
                (db.Task.title == title) &
                (db.Task.user_id == user_id) &
                (db.Task.status == status)
            )
        )

    @staticmethod
    def get_user_tasks_by_title_without_status(title, user_id):
        """Returns user tasks by title."""
        return TaskStorage._init_task(
            db.Task.select().where(
                (db.Task.title == title) & (db.Task.user_id == user_id)
            )
        )

    @staticmethod
    def get_user_task_by_id(task_id, user_id, status=Status.ACTIVE):
        """Returns user task by id."""
        return TaskStorage._init_task(
            db.Task.select().where(
                (db.Task.task_id == task_id) &
                (db.Task.user_id == user_id) &
                (db.Task.status == status)
            )
        )[0]

    @staticmethod
    def get_user_tasks_by_group(name, user_id, status, is_without_deadline=False):
        """Returns user tasks by group name."""
        name = '%' + name + '%'
        return TaskStorage._init_task(
            db.Task.select().where(
                (db.Task.user_id == user_id) &
                (db.Task.status == status) &
                (db.Task.group ** name) &
                (db.Task.deadline_date.is_null(is_without_deadline)) &
                (db.Task.parent_task_id.is_null(True))
            )
        )

    @staticmethod
    def delete_task(task_id):
        """Deletes user task."""
        TaskStorage._delete_task_recursive(task_id)

    @staticmethod
    def _delete_task_recursive(task_id):
        if task_id:
            for task in db.Task.select().where(db.Task.parent_task_id == task_id):
                TaskStorage._delete_task_recursive(task.task_id)
            TaskStorage._delete(task_id)

    @staticmethod
    def _delete(task_id):
        db.Task.delete().where(db.Task.task_id == task_id).execute()

    @staticmethod
    def update_task(task):
        """Updates task fields."""
        db.Task.update(
            title=task.title,
            deadline_date=task.deadline_date,
            priority=task.priority,
            group=task.group,
            description=task.description,
            access=task.access,
            status=task.status,
            related_tasks=task.related_tasks,
            edit_date=datetime.now()
        ).where(db.Task.task_id == task.id).execute()

    @staticmethod
    def _init_task(tasks):
        """
        Initializes tasks with parent task and subtasks if they exist.

        Returns:
            List of Task and/or Subtask objects with initialized parent_task and subtasks fields.
        """
        init_tasks = []
        for task in tasks:
            if task.parent_task_id:
                init_tasks.append(TaskStorage._add_parent_task(task, task.parent_task_id))
            else:
                init_tasks.append(TaskStorage._get_task_object(task))
        for task in init_tasks:
            TaskStorage._add_subtasks_recursive(task)
        return init_tasks

    @staticmethod
    def _add_parent_task(subtask, parent_task_id):
        """Returns Subtask with initialized parent_task field."""
        task = [db.Task.get(db.Task.task_id == parent_task_id)]
        parent = TaskStorage._init_task(task)[0]
        return TaskStorage._get_subtask_object(subtask, parent)

    @staticmethod
    def _add_subtasks_recursive(task):
        """Adds subtasks to the task recursively."""
        subtasks = TaskStorage._get_subtasks_by_parent_task(task)
        task.subtasks = subtasks
        for sub in task.subtasks:
            TaskStorage._add_subtasks_recursive(sub)

    @staticmethod
    def _get_subtasks_by_parent_task(task):
        """Returns list of subtasks objects for the task."""
        subtasks = db.Task.select().where(db.Task.parent_task_id == task.id)
        return [TaskStorage._get_subtask_object(sub, task) for sub in subtasks]

    @staticmethod
    def _get_task_object(task):
        return Task(
            title=task.title,
            deadline_date=task.deadline_date,
            priority=task.priority,
            group=task.group,
            description=task.description,
            id=task.task_id,
            create_date=task.create_date,
            related_tasks=task.related_tasks,
            access=task.access,
            status=task.status,
            edit_date=task.edit_date
        )

    @staticmethod
    def _get_subtask_object(subtask, parent_task):
        return Subtask(
            parent_task=parent_task,
            title=subtask.title,
            deadline_date=subtask.deadline_date,
            priority=subtask.priority,
            description=subtask.description,
            id=subtask.task_id,
            create_date=subtask.create_date,
            group=subtask.group,
            access=subtask.access,
            status=subtask.status,
            related_tasks=subtask.related_tasks
        )

    @staticmethod
    def update_status():
        """
        Check all tasks deadlines and update their status.

        Returns:
            List of failed tasks title.
        """
        failed_titles = []
        failed_tasks = db.Task.select().where(
            (db.Task.status == Status.ACTIVE) & (db.Task.deadline_date < datetime.now().date())
        )
        parents = []
        for task in failed_tasks:
            db.Task.update(
                status=Status.FAILED
            ).where(db.Task.task_id == task.task_id).execute()
            failed_titles.append(task.title)
            if task.parent_task_id:
                parents.append((task.parent_task_id, db.Task.get(db.Task.task_id == task.parent_task_id).title,))
        failed_titles.extend(TaskStorage._update_parents_tasks_status(parents))
        return failed_titles

    @staticmethod
    def _update_parents_tasks_status(parents):
        """
        Check if all subtasks of tasks is failed and if it is, update their status.

        Args:
            parents: List of tuples form (parent_task_id, parent_task_title).

        Returns:
            List of failed tasks titles.
        """
        failed = []
        for id, title in parents:
            if db.Task.select().where(
                            (db.Task.parent_task_id == id) & (db.Task.status == Status.FAILED)
            ).count() == db.Task.select().where(db.Task.parent_task_id == id).count():
                failed.append(title)
                db.Task.update(status=Status.FAILED).where(db.Task.task_id == id).execute()
        return failed


class UserStorage:
    """This class contains static methods that saves and deletes users in database, returns logins of all users."""

    @staticmethod
    def get_users():
        """Returns dictionary in which login is key and user id is value."""
        users = db.User.select()
        return {user.login: user.user_id for user in users}

    @staticmethod
    def save_user(user):
        """Saves user info in database."""
        db.User.create(
            name=user.name,
            login=user.login,
            email=user.email
        )

    @staticmethod
    def delete_user(user_id):
        """Deletes all user info."""
        db.EventPlan.delete().where(db.EventPlan.user_id == user_id).execute()
        db.EventPlanDays.delete().where(db.EventPlanDays.user_id == user_id).execute()
        db.Event.delete().where(db.Event.user_id == user_id).execute()
        db.Task.delete().where(db.Task.user_id == user_id).execute()
        db.User.delete().where(db.User.user_id == user_id).execute()


class EventStorage:
    """
    This class contains static methods that allows save, update, delete events in database,
    returns events by specified parameters from database.
    """
    @staticmethod
    def save_event(event, user_id):
        """Save user event in database."""
        db.Event.create(
            title=event.title,
            create_date=event.create_date,
            date=event.date,
            time=event.time,
            priority=event.priority,
            group=event.group,
            description=event.description,
            user_id=user_id,
            access=event.access,
            event_plan_id=event.plan_id,
            edit_date=event.edit_date
        )
        return db.Event.select().order_by(db.Event.event_id.desc()).get()

    @staticmethod
    def get_user_events_by_date(date, user_id):
        """Returns user events by date."""
        return [EventStorage._get_event_object(event) for event in db.Event.select().where(
            (db.Event.user_id == user_id) &
            (db.Event.date == date) &
            (db.Event.event_plan_id.is_null(True))
        )]

    @staticmethod
    def get_user_events_by_title(title, user_id):
        """Returns user events by title."""
        return [EventStorage._get_event_object(event) for event in db.Event.select().where(
            (db.Event.title == title) & (db.Event.user_id == user_id)
        )]

    @staticmethod
    def get_user_events_by_group(name, user_id):
        """Returns user events by group name."""
        name = '%' + name + '%'
        return [EventStorage._get_event_object(event) for event in db.Event.select().where(
            (db.Event.user_id == user_id) & (db.Event.group ** name)
        )]

    @staticmethod
    def delete_event(event_id):
        """Deletes user events."""

        # if it is plan event before deleting event delete event plan
        db.EventPlan.delete().where(db.EventPlan.event_id == event_id).execute()
        db.EventPlanDays.delete().where(db.EventPlanDays.event_id == event_id).execute()
        db.Event.delete().where(db.Event.event_id == event_id).execute()

    @staticmethod
    def update_event(event):
        """Updates event fields."""
        db.Event.update(
            title=event.title,
            date=event.date,
            time=event.time,
            priority=event.priority,
            group=event.group,
            description=event.description,
            access=event.access,
            edit_date=datetime.now()
        ).where(db.Event.event_id == event.id).execute()

    @staticmethod
    def _get_event_object(event):
        return Event(
            title=event.title,
            date=event.date,
            time=event.time,
            create_date=event.create_date,
            priority=event.priority,
            description=event.description,
            id=event.event_id,
            access=event.access,
            plan_id=event.event_plan_id,
            edit_date=event.edit_date
        )


class GroupStorage:

    """This class implements static method that returns all groups of tasks and events."""

    @staticmethod
    def get_groups(user_id):
        """Returns all groups of tasks and events."""
        groups = set()
        for task in db.Task.select().where(db.Task.user_id == user_id):
            if task.group:
                gr = tools.parse_group(task.group)
                groups.update(gr)
        for event in db.Event.select().where(db.Event.user_id == user_id):
            if event.group:
                gr = tools.parse_group(event.group)
                groups.update(gr)
        return groups


class PlanStorage:
    """
    This class contains static methods that allows save, update, delete event plans in database,
    returns event plans by specified parameters from database.
    """
    @staticmethod
    def save_event_plan(event_plan, user_id):
        """Saves user EventPlan in database."""
        db.EventPlan.create(
            event_id=event_plan.event.id,
            event_plan_id=event_plan.id,
            delta_date=event_plan.delta_date,
            first_date=event_plan.first_date,
            user_id=user_id,
            edit_date=event_plan.edit_date
        )

    @staticmethod
    def save_event_plan_days(event_plan_days, user_id):
        """Saves user EventPlanDays in database."""
        db.EventPlanDays.create(
            event_id=event_plan_days.event.id,
            event_plan_days_id=event_plan_days.id,
            days=event_plan_days.days,
            first_date=event_plan_days.first_date,
            user_id=user_id,
            edit_date=event_plan_days.edit_date
        )

    @staticmethod
    def get_user_event_plans(user_id):
        """Returns user event plans."""
        plans = []
        result = db.EventPlan.select().join(db.Event).where(db.Event.event_plan_id.is_null(False) &
                                                            (db.Event.user_id == user_id))
        for plan in result:
            event = plan.event
            plans.append(PlanStorage._get_event_plan_object(plan, event))

        result = db.EventPlanDays.select().join(db.Event).where(db.Event.event_plan_id.is_null(False) &
                                                                (db.Event.user_id == user_id))
        for plan in result:
            event = EventStorage._get_event_object(plan.event)
            plans.append(PlanStorage._get_event_plan_days_object(plan, event))
        return plans

    @staticmethod
    def get_user_event_plans_by_title(title, user_id):
        """Returns user event plans by title."""
        plans = []
        result = db.EventPlan.select().join(db.Event).where(
            db.Event.event_plan_id.is_null(False) & (db.Event.user_id == user_id) & (db.Event.title == title)
        )
        for plan in result:
            event = plan.event
            plans.append(PlanStorage._get_event_plan_object(plan, event))

        result = db.EventPlanDays.select().join(db.Event).where(
            db.Event.event_plan_id.is_null(False) & (db.Event.user_id == user_id) & (db.Event.title == title)
        )
        for plan in result:
            event = EventStorage._get_event_object(plan.event)
            plans.append(PlanStorage._get_event_plan_days_object(plan, event))
        return plans

    @staticmethod
    def update_event_plan(event_plan):
        """Updates user EventPlan fields."""
        db.EventPlan.update(
            delta_date=event_plan.delta_date,
            first_date=event_plan.first_date,
            edit_date=datetime.now()
        ).where(db.EventPlan.event_plan_id == event_plan.id).execute()

    @staticmethod
    def update_event_plan_days(event_plan_days):
        """Updates user EventPlanDays fields."""
        db.EventPlanDays.update(
            days=event_plan_days.days,
            first_date=event_plan_days.first_date,
            edit_date=datetime.now()
        ).where(db.EventPlanDays.event_plan_days_id == event_plan_days.id).execute()

    @staticmethod
    def _get_event_plan_object(event_plan, event):
        return EventPlan(
            id=event_plan.event_plan_id,
            event=event,
            delta_date=event_plan.delta_date,
            first_date=event_plan.first_date,
            edit_date=event_plan.edit_date,
        )

    @staticmethod
    def _get_event_plan_days_object(event_plan_days, event):
        return EventPlanDays(
            id=event_plan_days.id,
            event=event,
            days=event_plan_days.days,
            first_date=event_plan_days.first_date,
            edit_date=event_plan_days.edit_date
        )
