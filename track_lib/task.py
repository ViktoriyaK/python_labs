"""This module contains Task and Subtask classes."""


import datetime
from track_lib.enums import Priority, Access, Status


class Task:
    """Class for storing users tasks."""
    def __init__(
            self,
            title,
            deadline_date=None,
            priority=None,
            group=None,
            description=None,
            id=None,
            create_date=datetime.datetime.now().date(),
            subtasks=None,
            related_tasks=None,
            access=Access.PRIVATE,
            status=Status.ACTIVE,
            edit_date=datetime.datetime.now()
    ):
        """Initializes task fields."""

        if priority is None:
            priority = Priority.NO
        if subtasks is None:
            subtasks = []

        self.id = id
        self.title = title
        self.create_date = create_date
        self.deadline_date = deadline_date
        self.priority = priority
        self.group = group
        self.description = description
        self.subtasks = subtasks
        self.access = access
        self.status = status
        self.related_tasks = related_tasks
        self.edit_date = edit_date


class Subtask(Task):
    """Class for storing subtasks."""
    def __init__(
            self,
            parent_task,
            title,
            deadline_date=None,
            priority=None,
            description=None,
            id=None,
            create_date=datetime.datetime.now().date(),
            subtasks=None,
            group=None,
            access=Access.PRIVATE,
            status=Status.ACTIVE,
            related_tasks=None
    ):
        """Initializes subtask fields."""

        Task.__init__(
            self,
            title,
            deadline_date,
            priority,
            group,
            description,
            id,
            create_date,
            subtasks,
            related_tasks,
            access,
            status
        )
        self.parent_task = parent_task

    @staticmethod
    def create_from_task(parent_task, task):
        """Return Subtask object that creates from task fields."""
        return Subtask(
            parent_task,
            task.title,
            task.deadline_date,
            task.priority,
            task.description,
            task.id,
            task.create_date,
            task.subtasks,
            task.group,
            task.access)
