"""
This module contains EventPlan and EventPlanDays classes that allows add an event once and repeat it
according to the specified conditions.
"""


import uuid
from .tools import parse_days
from datetime import datetime


class EventPlan:
    """Class that allows create event that will be repeated with a period delta_date."""

    def __init__(self, event, delta_date, first_date, id=uuid.uuid4(), edit_date=datetime.now()):
        """Initializes event plan fields."""
        self.id = id
        self.event = event
        self.delta_date = delta_date
        self.first_date = first_date
        self.edit_date = edit_date

    def is_today(self, date):
        """Check is there a plan event on this date."""
        if date < self.first_date:
            return False
        temp_date = self.first_date
        while temp_date < date:
            temp_date += self.delta_date
        if temp_date == date:
            return True
        return False


class EventPlanDays:

    """Class that allows create event that will be repeated specified days every week."""

    def __init__(self, event, days, first_date, id=uuid.uuid4(), edit_date=datetime.now()):
        """Initializes event plan fields."""
        self.id = id
        self.event = event
        self.days = days
        self.first_date = first_date
        self.edit_date = edit_date

    def is_today(self, date):
        """Check is therre a plan event on this date."""
        if date < self.first_date:
            return False
        d = parse_days(self.days)
        if date.weekday() in d:
            return True
        return False
