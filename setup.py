from setuptools import setup, find_packages

setup(
    name='track',
    version='0.1',
    author='ViktoriyaK',
    packages=find_packages(),
    py_modules=['db', 'task', 'tools', 'event', 'user', 'tracker', 'console'],
    entry_points='''
        [console_scripts]
        track=console_lib.tracker:cli''',
    install_requires=['click']
)
