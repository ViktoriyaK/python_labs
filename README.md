track
============
Track is a command line client for creating tasks and events lists written in Python that can be used on Linux. It allows you to:

* create tasks, events; it's very simple since you do not need to specify all the characteristics, only the title is required; after adding you can edit tasks and events
* specify a group for tasks and events that then you can view the contents of groups
* create a plan that will create something specified by you according to conditions
* specify related tasks
* add subtasks
* specify priority for tasks and events
* change access to you tasks and events so that then other users have read and/or write access

### Installation
You can install track as a python script.

#### Downloading and installing from source

    # Download the repository.
    $ git clone https://ViktoriyaK@bitbucket.org/ViktoriyaK/python_labs.git

    $ cd python_labs

    # Installation
    $ [sudo] python3 setup.py install

### Running tests

    $ python3 tests

### How to use

Usage: track [OPTIONS] COMMAND [ARGS]...

#### Track commands:
 * day - show tasks and events on this day
 * event - command for work with events
 * group - show content of the group
 * groups - show all groups
 * plan - command for work with plans
 * task - command for work with tasks
 * today - show tasks and events on today
 * user - command for work with users
 * users - show logins of all users