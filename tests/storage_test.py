import unittest
from track_lib import db
from track_lib.storage import (
    UserStorage,
    TaskStorage,
    EventStorage,
    PlanStorage,
    GroupStorage
)
from track_lib.enums import Status
from track_lib.user import User
from track_lib.task import Task
from track_lib.event import Event
from track_lib.plan import EventPlan
from datetime import date, time
from dateutil.relativedelta import relativedelta
import uuid


database = ':memory:'

users = (
    User('User1', 'user1', 'email1'),
    User('User2', 'user2', 'email2'),
    User('User3', 'user3', 'email3')
)

tasks = (
    Task("task1", date(2018, 3, 20), group="group", status=Status.ACTIVE, id=1),
    Task("task2", group="group2", status=Status.ACTIVE, id=2),
    Task("task3", date(2018, 3, 20), id=3),
    Task("task4", date(2018, 3, 20), status=Status.READY, id=4),
    Task("task", status=Status.READY, id=5),
    Task("task", status=Status.ACTIVE, id=6),
    Task("task", deadline_date=date(2018, 3, 20), id=7)
)

events = (
    Event("event1", date(2018, 3, 20), time(10, 0), group="group", id=1),
    Event("event2", date(2018, 3, 21), time(10, 0), group="group", id=2),
    Event("event3", date(2018, 3, 20), time(10, 0), id=3),
    Event("event4", date(2018, 3, 20), time(0, 0), id=4),
    Event("event1", date(2018, 3, 21), time(10, 0), group="group1", id=5)
)

pids1 = (
    uuid.uuid4(),
    uuid.uuid4()
)

plans = (
    EventPlan(events[0], relativedelta(days=2), date(2018, 3, 20), id=pids1[0]),
    EventPlan(events[1], relativedelta(months=1), date(2018, 3, 20), id=pids1[1])
)

db.Adapter(':memory:')


class TestStorage(unittest.TestCase):
    def test_user_storage(self):
        before_adding_count = db.User.select().count()
        for user in users:
            UserStorage.save_user(user)
        after_adding_count = db.User.select().count()

        self.assertEqual(before_adding_count + len(users), after_adding_count)
        self.assertEqual(users[0].email, db.User.get(db.User.login == users[0].login).email)
        self.assertEqual(users[1].name, db.User.get(db.User.login == users[1].login).name)

        query_users = UserStorage.get_users()
        for user in users:
            self.assertIn(user.login, query_users)

        before_deleting_count = db.User.select().count()
        UserStorage.delete_user(len(users))
        after_deleting_count = db.User.select().count()
        self.assertEqual(before_deleting_count - 1, after_deleting_count)

        query_users = UserStorage.get_users()
        self.assertNotIn(users[-1].login, query_users)

    def test_saving_task(self):
        before_adding_count = db.Task.select().count()
        TaskStorage.save_task(tasks[0], 1)
        after_adding_count = db.Task.select().count()

        self.assertEqual(before_adding_count + 1, after_adding_count)
        self.assertEqual("task1", db.Task.get(db.Task.title == "task1").title)

    def test_deleting_task(self):
        TaskStorage.save_task(tasks[0], tasks[0].id)
        TaskStorage.delete_task(1)
        self.assertEqual(0, db.Task.select().count())

    def test_updating_task(self):
        TaskStorage.save_task(tasks[0], tasks[0].id)
        tasks[0].description = "description1"
        TaskStorage.update_task(tasks[0])
        self.assertEqual(tasks[0].description, db.Task.get().description)

    def test_geting_user_task_by_id(self):
        TaskStorage.save_task(tasks[0], 1)
        task = TaskStorage.get_user_task_by_id(tasks[0].id, 1, tasks[0].status)
        self.assertEqual(tasks[0].title, task.title)

    def test_geting_user_tasks_by_date(self):
        for task in tasks:
            TaskStorage.save_task(task, 1)

        result = TaskStorage.get_user_tasks_by_date(date(2018, 3, 20), 1, Status.ACTIVE)
        for task in result:
            self.assertEqual(date(2018, 3, 20), task.deadline_date)

    def test_geting_tasks_by_title(self):
        for task in tasks:
            TaskStorage.save_task(task, 1)

        result = TaskStorage.get_user_tasks_by_title("tast", 1)
        for task in result:
            self.assertEqual("test", task.title)

    def test_saving_event(self):
        before_adding_count = db.Event.select().count()
        EventStorage.save_event(events[0], 1)
        after_adding_count = db.Event.select().count()

        self.assertEqual(before_adding_count + 1, after_adding_count)
        self.assertEqual("event1", db.Event.get(db.Event.title == "event1").title)

    def test_deleting_event(self):
        EventStorage.save_event(events[0], events[0].id)
        EventStorage.delete_event(1)
        self.assertEqual(0, db.Event.select().count())

    def test_updating_event(self):
        EventStorage.save_event(events[0], events[0].id)
        events[0].description = "description1"
        EventStorage.update_event(events[0])
        self.assertEqual(events[0].description, db.Event.get().description)

    def test_geting_user_events_by_date(self):
        for event in events:
            EventStorage.save_event(event, 1)

        result = EventStorage.get_user_events_by_date(date(2018, 3, 20), 1)
        for event in result:
            self.assertEqual(date(2018, 3, 20), event.date)

    def test_geting_events_by_title(self):
        result = EventStorage.get_user_events_by_title("event1", 1)
        for event in result:
            self.assertEqual("event1", event.title)

    def test_saving_event_plan(self):
        before_adding_count = db.EventPlan.select().count()
        PlanStorage.save_event_plan(plans[0], 1)
        after_adding_count = db.EventPlan.select().count()

        self.assertEqual(before_adding_count + 1, after_adding_count)
        self.assertEqual("event1", db.EventPlan.get(db.EventPlan.event_plan_id == pids1[0]).event.title)

    def test_updating_event_plan(self):
        PlanStorage.save_event_plan(plans[0], 1)
        plans[0].delta_date = relativedelta(days=3)
        PlanStorage.update_event_plan(plans[0])
        self.assertEqual(plans[0].delta_date, db.EventPlan.get().delta_date)

    def test_geting_groups(self):
        for task in tasks:
            TaskStorage.save_task(task, 1)

        for event in events:
            EventStorage.save_event(event, 1)

        result_groups = GroupStorage.get_groups(1)
        self.assertEqual(result_groups, {"group", "group1", "group2"})


if __name__ == '__main__':
    unittest.main()
