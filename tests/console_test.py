# -*- coding: utf-8 -*-

from unittest.mock import patch
import unittest
from datetime import date
from console_lib import console, console_tools
from track_lib.enums import Priority, Access


class TestConsole(unittest.TestCase):
    def test_input_index(self):
        values = (
            ('1', 12, 1),
            ('12', 13, 12),
            ('122', 123, 122),
            ('2', 2, 2)
        )
        for i, length, result in values:
            self.assertEqual(result, console_tools.input_index(i, length))

    def test_input_title(self):
        user_input = (
            ("title",),
            ("", "ere"),
            ("", "", "", "title")
        )
        results = (
            "title",
            "ere",
            "title"
        )
        for i in range(len(user_input)):
            with patch('builtins.input', side_effect=user_input[i]):
                self.assertEqual(results[i], console_tools.input_title())

    def test_input_date(self):
        user_input = ["12 03 2018"]
        result = date(2018, 3, 12)

        with patch('builtins.input', side_effect=user_input):
            self.assertEqual(result, console_tools.input_date("test"))

    def test_input_priority(self):
        user_input = (
            [""],
            ["0"],
            ["1"],
            ["2"],
            ["3"]
        )
        results = (
            Priority.NO,
            Priority.NO,
            Priority.LOW,
            Priority.MEDIUM,
            Priority.HIGH
        )
        for i in range(len(user_input)):
            with patch('builtins.input', side_effect=user_input[i]):
                self.assertEqual(results[i], console_tools.input_priority())

    def test_input_access(self):
        user_input = (
            [""],
            ["p"],
            ["r"],
            ["w"]
        )
        results = (
            Access.PRIVATE,
            Access.PRIVATE,
            Access.READE,
            Access.WRITE
        )
        for i in range(len(user_input)):
            with patch('builtins.input', side_effect=user_input[i]):
                self.assertEqual(results[i], console_tools.input_access())


if __name__ == '__main__':
    unittest.main()
