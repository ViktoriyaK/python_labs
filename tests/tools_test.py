# -*- coding: utf-8 -*-

import unittest
from track_lib import tools
from datetime import date, time


class TestTools(unittest.TestCase):
    def test_empty_to_none(self):
        test_values = (
            ("", None),
            ("no", None),
            ("нет", None),
            ("yes", "yes"),
            ("1", "1")
        )
        for value, result in test_values:
            self.assertEqual(result, tools.empty_to_none(value))

    def test_to_date(self):
        test_values = (
            ("21 03 2018", date(2018, 3, 21)),
            ("1 1 2000", date(2000, 1, 1)),
            ("31 05 2100", date(2100, 5, 31)),
            (date(2018, 1, 12), date(2018, 1, 12)),
            ("", None),
            (None, None)
        )
        for value, result in test_values:
            self.assertEqual(result, tools.to_date(value))

    def test_to_date_bad_input(self):
        test_values = ("12", "12 12 12", "12 2018", "a", "200 02 2019", "20 13 2018", "0 1 2018")
        for value in test_values:
            self.assertRaises(ValueError, tools.to_date, value)

    def test_to_time(self):
        test_values = (
            ("0:0", time(0, 0)),
            ("23:59", time(23, 59)),
            ("", None),
            (None, None)
        )
        for value, result in test_values:
            self.assertEqual(result, tools.to_time(value))


if __name__ == '__main__':
    unittest.main()
