# -*- coding: utf-8 -*-

import unittest
from track_lib.plan import EventPlan, EventPlanDays
from track_lib.event import Event
from datetime import date, time
from dateutil.relativedelta import relativedelta


class TestPlan(unittest.TestCase):
    def test_is_today_plan_event(self):
        event = Event("test", date(2018, 1, 1), time(11, 11))

        test_plans = (
            EventPlan(event, relativedelta(days=1), first_date=event.date),
            EventPlan(event, relativedelta(days=2), first_date=event.date),
            EventPlan(event, relativedelta(days=20), first_date=event.date),
            EventPlan(event, relativedelta(months=1), first_date=event.date),
            EventPlan(event, relativedelta(months=2), first_date=event.date),
            EventPlan(event, relativedelta(months=6), first_date=event.date),
            EventPlan(event, relativedelta(years=1), first_date=event.date),
            EventPlan(event, relativedelta(years=5), first_date=event.date),
        )

        correct_dates = (
            (date(2018, 1, 1), date(2018, 1, 2), date(2018, 1, 29), date(2019, 1, 1), date(2018, 6, 10)),
            (date(2018, 1, 1), date(2018, 1, 3), date(2018, 1, 29), date(2018, 6, 4)),
            (date(2018, 1, 21), date(2018, 1, 1), date(2018, 2, 10), date(2018, 3, 2)),
            (date(2018, 1, 1), date(2018, 2, 1), date(2018, 5, 1), date(2019, 1, 1)),
            (date(2018, 1, 1), date(2018, 3, 1), date(2018, 7, 1), date(2018, 1, 1), date(2019, 1, 1)),
            (date(2018, 1, 1), date(2018, 7, 1), date(2019, 1, 1)),
            (date(2018, 1, 1), date(2019, 1, 1), date(2100, 1, 1)),
            (date(2018, 1, 1), date(2023, 1, 1), date(2028, 1, 1))
        )

        incorrect_dates = (
            (date(2017, 12, 31), date(2017, 1, 1), date(2000, 1, 1)),
            (date(2017, 12, 31), date(2017, 1, 2), date(2017, 1, 20)),
            (date(2017, 12, 31), date(2017, 1, 20), date(2017, 1, 22)),
            (date(2017, 12, 31), date(2017, 1, 31), date(2017, 2, 11)),
            (date(2017, 12, 31), date(2017, 2, 1), date(2017, 12, 1)),
            (date(2017, 12, 31), date(2017, 6, 1), date(2017, 12, 1)),
            (date(2017, 12, 31), date(2018, 1, 2), date(2019, 2, 1)),
            (date(2017, 12, 31), date(2019, 1, 1), date(2027, 1, 1))
        )

        for i in range(len(test_plans)):
            for test_date in correct_dates[i]:
                self.assertEqual(True, test_plans[i].is_today(test_date))

        for i in range(len(test_plans)):
            for test_date in incorrect_dates[i]:
                self.assertEqual(False, test_plans[i].is_today(test_date))

    def test_is_today_plan_days_event(self):
        event = Event("test", date(2018, 1, 1), time(11, 11))

        test_plans = (
            EventPlanDays(event, "0", first_date=event.date),
            EventPlanDays(event, "6", first_date=event.date),
            EventPlanDays(event, "0 1", first_date=event.date),
            EventPlanDays(event, "0 4", first_date=event.date),
            EventPlanDays(event, "0 1 2 3 4 5 6", first_date=event.date),
        )

        correct_dates = (
            (date(2018, 1, 1), date(2018, 6, 11), date(2018, 12, 3)),
            (date(2018, 1, 7), date(2018, 3, 25), date(2019, 1, 6)),
            (date(2018, 1, 1), date(2018, 5, 28), date(2018, 12, 3)),
            (date(2018, 1, 1), date(2018, 1, 26), date(2018, 11, 9)),
            (date(2018, 1, 1), date(2018, 1, 2), date(2018, 1, 3), date(2018, 1, 4), date(2018, 1, 5), date(2018, 1, 6),
             date(2018, 1, 7), date(2018, 1, 8))
        )

        incorrect_dates = (
            (date(2018, 1, 2), date(2018, 6, 10), date(2018, 12, 4)),
            (date(2018, 1, 6), date(2017, 3, 24), date(2019, 1, 1)),
            (date(2018, 1, 3), date(2017, 5, 27), date(2017, 12, 3)),
            (date(2017, 12, 31), date(2018, 1, 27), date(2018, 11, 8)),
            (date(2017, 12, 31), date(2016, 1, 2), date(2000, 1, 3))
        )

        for i in range(len(test_plans)):
            for test_date in correct_dates[i]:
                self.assertEqual(True, test_plans[i].is_today(test_date))

        for i in range(len(test_plans)):
            for test_date in incorrect_dates[i]:
                self.assertEqual(False, test_plans[i].is_today(test_date))


if __name__ == '__main__':
    unittest.main()
