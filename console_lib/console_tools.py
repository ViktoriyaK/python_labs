"""
This module contains functions for input information by user, displaying information about tasks, events, event plans,
also contains validation user input functions.
"""


import click
import logging
import sys
from track_lib.plan import EventPlan
from track_lib.task import Task
from track_lib.event import Event
from track_lib.tools import *
from console_lib import config_parser


logger = logging.getLogger(__name__)


def get_task():
    """Input information about new task by user.

    In the case of incorrect input exits with code 1.

    Returns:
        New Task object.
    """
    title = input("Заголовок*: ")
    while title == "":
        print("Заголовок не должен быть пустым.")
        title = input("Заголовок*: ")
    try:
        deadline = to_date(input("Дедлайн: "))
        priority = str_to_priority(input("Приоритет (от 0 - наименьший до 3 - наибольший): "))
        group = empty_to_none(input("Группа: "))
        description = empty_to_none(input("Описание: "))
    except ValueError as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    task = Task(title=title, deadline_date=deadline, priority=priority, group=group, description=description)
    return task


def get_event():
    """Input information about new event by user.

    In the case of incorrect input exits with code 1.

    Returns:
        New Event object.
    """
    title = input("Заголовок*: ")
    while title == "":
        print("Заголовок не должен быть пустым.")
        title = input("Заголовок: ")
    try:
        date = to_date(input("Дата*: "))
        while date is None:
            print("Пожалуйста, введите корректную дату.")
            date = to_date(input("Дата*: "))
        time = to_time(input("Время*: "))
        while time is None:
            print("Пожалуйста, введите корректное время.")
            time = to_time(input("Время*: "))
        priority = str_to_priority(input("Приоритет (от 0 - наименьший до 3 - наибольший): "))
        group = empty_to_none(input("Группа: "))
        description = empty_to_none(input("Описание: "))
    except ValueError as error:
        print(error, file=sys.stderr)
        sys.exit(1)
    return Event(title=title, date=date, time=time, priority=priority, group=group, description=description)


def get_task_index_from_user(tasks, title):
    """Returns index of task in tasks list selected by user.

    Args:
        tasks: List of tasks.
        title: The title of tasks.

    Returns:
        Index of selected task.
    """
    if not tasks:
        print("Задача с заголовком '{}' не найдена.".format(title))
        sys.exit(0)
    elif len(tasks) == 1:
        return 0
    else:
        click.secho("С данным заголовком найдено несколько задач", bold=True)
        i = 1
        while i <= len(tasks):
            if isinstance(tasks[i - 1], Task):
                type_name = "Задача №"
            else:
                type_name = "Подзадча №"
            click.secho(type_name + str(i), bg='cyan', fg='white', bold=True)
            display_task_full(tasks[i - 1], show_access=True, show_create_date=True, show_status=True,
                              show_edit_date=True)
            i += 1
        index = input_index(input("Введите номер задачи: "), len(tasks))
        return index - 1


def get_event_index_from_user(events, title):
    """Returns index of event in events list selected by user.

    Args:
        events: List of events.
        title: The title of events.

    Returns:
        Index of selected event.
    """
    if not events:
        print("Мероприятие с заголовком '{}' не найдено.".format(title))
        sys.exit(0)
    elif len(events) == 1:
        return 0
    else:
        click.secho("С данным заголовком найдено несколько мероприятий", bold=True)
        i = 1
        while i <= len(events):
            click.secho('Мероприятие №{}'.format(i), bg='cyan', fg='white', bold=True)
            display_event_full(events[i - 1], show_create_date=True, show_edit_date=True)
            i += 1
        index = input_index(input("Введите номер мероприятия: "), len(events))
        return index - 1


def get_event_plan_index_from_user(plans, title):
    """Returns index of event plan in event plans list selected by user.

    Args:
        plans: List of event plans.
        title: The title of event plans.

    Returns:
        Index of selected event plan.
    """
    if not plans:
        print("Плановое ероприятие с заголовком '{}' не найдено.".format(title))
        sys.exit(0)
    if len(plans) == 1:
        return 0
    else:
        click.secho("С данным заголовком найдено несколько мероприятий", bold=True)
        i = 1
        while i <= len(plans):
            click.secho('Мероприятие №{}'.format(i), bg='cyan', fg='white', bold=True)
            display_plan(plans[i - 1])
            i += 1
        index = input_index(input("Введите номер мероприятия: "), len(plans))
        if index:
            return index - 1


def display_short_task(task, show_deadline=False):
    """Display task in short format."""
    if show_deadline:
        click.secho('{}, дедлайн - {}'.format(task.title, str_to_display(date_to_str(task.deadline_date))),
                    fg=get_color(task.priority))
    else:
        click.secho('{}'.format(task.title), fg=get_color(task.priority))
    _display_subtasks(task, task.status)


def _display_subtasks(task, status=None, h=1, check_access=False):
    """Display all subtasks of the task."""
    if not task.subtasks:
        return
    is_displayed = False
    for sub in task.subtasks:
        if (sub.status == status or status is None) and ((not check_access) or (check_access and
           (sub.access == Access.READE or sub.access == Access.WRITE))):
            click.secho(
                '    ' * h + '*{}, дедлайн - {}'.format(sub.title, str_to_display(date_to_str(sub.deadline_date))),
                fg=get_color(sub.priority))
            is_displayed = True
            _display_subtasks(sub, status, h=h + 1, check_access=check_access)
    if not is_displayed and check_access:
        print("Доступ закрыт.")


def display_event_short(event):
    """Display event in short format."""
    click.secho('{} - {}'.format(time_to_str(event.time), event.title), fg=get_color(event.priority))


def display_event_full(event, show_create_date=False, show_edit_date=False):
    """Display full event information."""
    if event.plan_id:
        click.echo(click.style('Плановое', bold=True))
    click.echo(click.style('1. Заголовок: ', bold=True) + event.title)
    click.echo(click.style('2. Дата: ', bold=True) + date_to_str(event.date))
    click.echo(click.style('3. Время: ', bold=True) + time_to_str(event.time))
    click.echo(click.style('4. Приоритет: ', bold=True) + (priority_to_str(event.priority)))
    click.echo(click.style('5. Группа: ', bold=True) + str_to_display(event.group))
    click.echo(click.style('6. Описание: ', bold=True) + str_to_display(event.description))
    click.echo(click.style('7. Доступ: ', bold=True) + str_to_display(str(event.access)))
    if show_create_date:
        click.echo(click.style('8. Дата создания: ', bold=True) + date_to_str(event.create_date))
    if show_edit_date:
        click.echo(click.style('9. Дата и время последнего изменения: ', bold=True) + date_time_to_str(event.edit_date))


def display_task_full(task, show_status=False, show_create_date=False, show_group=True, show_access=True,
                      show_subtasks=True, check_access=False, show_edit_date=False):
    """Display full task information."""
    click.echo(click.style('1. Заголовок: ', bold=True) + task.title)
    click.echo(click.style('2. Дедлайн: ', bold=True) + str_to_display(date_to_str(task.deadline_date)))
    click.echo(click.style('3. Приоритет: ', bold=True) + (priority_to_str(task.priority)))
    click.echo(click.style('4. Описание: ', bold=True) + str_to_display(task.description))
    click.echo(click.style('5. Зависимые задачи: ', bold=True) + str_to_display(task.related_tasks))
    if show_group:
        click.echo(click.style('6. Группа: ', bold=True) + str_to_display(task.group))
    if show_access:
        click.echo(click.style('7. Доступ: ', bold=True) + str_to_display(str(task.access)))
    if show_create_date:
        click.echo(click.style('8. Дата создания: ', bold=True) + (date_to_str(task.create_date)))
    if show_status:
        click.echo(click.style('9. Статус: ', bold=True) + str_to_display(str(task.status)))
    if show_edit_date:
        click.echo(click.style('10. Дата и время последнего изменения: ', bold=True) + (date_time_to_str
                                                                                        (task.edit_date)))
    if show_subtasks:
        click.secho("11. Подзадачи:", bold=True)
        if not task.subtasks:
            print("    Нет")
            return
        _display_subtasks(task, check_access=check_access)



def _display_plan_event(event, show_group=True, show_date=True):
    """Display plan event information."""
    click.echo(click.style('Заголовок: ', bold=True) + event.title)
    if show_date:
        click.echo(click.style('Дата: ', bold=True) + (date_to_str(event.date)))
    click.echo(click.style('Время: ', bold=True) + (time_to_str(event.time)))
    click.echo(click.style('Приоритет: ', bold=True) + (priority_to_str(event.priority)))
    if show_group:
        click.echo(click.style('Группа: ', bold=True) + str_to_display(event.group))
    click.echo(click.style('Описание: ', bold=True) + str_to_display(event.description))
    click.echo(click.style('Доступ: ', bold=True) + str_to_display(str(event.access)))


def display_lists(events, tasks, tasks_without_deadline, show_deadline=False):
    """Display lists of events, tasks and tasks without deadline."""
    if events:
        click.secho('Мероприятия:                        ', bg='cyan', fg='white', bold=True)
        for event in events:
            display_event_short(event)
    if tasks:
        click.secho('Задачи:                             ', bg='blue', fg='white', bold=True)
        for task in tasks:
            display_short_task(task, show_deadline)
    if tasks_without_deadline:
        click.secho('Задачи без дедлайна:                ', bg='white', fg='black', bold=True)
        for task in tasks_without_deadline:
            display_short_task(task, show_deadline)


def display_plan(plan, show_event=True):
    """Display plan event information."""
    click.secho('План', bg='green', fg='white', bold=True)
    click.echo(click.style('Начальная дата: ', bold=True) + date_to_str(plan.first_date))
    if isinstance(plan, EventPlan):
        click.echo(click.style('Промежуток повторения: ', bold=True) + rel_to_display(plan.delta_date))
    else:
        click.echo(click.style('Дни повторений: ', bold=True) + plan_days_to_display(plan.days))
    if show_event:
        _display_plan_event(plan.event, show_group=True, show_date=False)


def input_index(x, length):
    """Returns a validate index.

    If index is not correct, exit with code 1.

    Args:
        x: The index.
        length: The max value of index.
    Returns:
        Correct index value.
    """
    try:
        index = int(x)
        if index <= 0 or index > length:
            raise ValueError
    except ValueError:
        print("Введено неверное значение номера - '{}'.".format(x), file=sys.stderr)
        logger.error("Entered an invalid number - '{}'".format(x), exc_info=True)
        sys.exit(1)
    return index


def input_title():
    """Returns not empty title."""
    new_title = input("Новый заголовок: ")
    while new_title == "":
        print("Заголовок не должен быть пустым.")
        new_title = input("Новый заголовок: ")
    return new_title


def input_date(input_str):
    """Returns a valid date object."""
    new_deadline = empty_to_none(input(input_str))
    try:
        new_deadline = to_date(new_deadline)
    except ValueError:
        logger.error("Entered an invalid date format - '{}'".format(new_deadline), exc_info=True)
        print("Неверный формат даты - '{}'.".format(new_deadline), file=sys.stderr)
        sys.exit(1)
    return new_deadline


def input_time():
    """Returns a valid time object."""
    new_t = empty_to_none(input("Новое время: "))
    try:
        new_t = to_time(new_t)
        if new_t is None:
            raise ValueError
    except ValueError:
        logger.error("Entered an invalid time format - '{}'".format(new_t), exc_info=True)
        print("Неверный формат времени - '{}'.".format(new_t), file=sys.stderr)
        sys.exit(1)
    return new_t


def input_priority():
    """Returns Priority object."""
    try:
        s = input("Новый приоритет (от 0 до 3): ")
        new_priority = str_to_priority(s)
    except ValueError:
        logger.error("Entered an invalid priority value - '{}'".format(s), exc_info=True)
        print("Недопустимое значение приоритета - '{}.'".format(s), file=sys.stderr)
        sys.exit(1)
    return new_priority


def input_access():
    """Returns Access object."""
    print("p - приватный\n"
    "r - разрешить другим пользователям просматривать\n"
    "w - разрешить другим пользователям просматривать и редактировать")
    new_access = input("Новый уровень доступа: ")
    try:
        new_access_ = str_to_access(new_access)
    except ValueError:
        logger.error("Entered an invalid access level - '{}'".format(new_access), exc_info=True)
        print("Некорректный ввод доступа - '{}'".format(new_access), file=sys.stderr)
        sys.exit(1)
    return new_access_


def input_week_days():
    """Returns a valid str that contains numbers of week days."""
    try:
        i = input("""Выберите дни недели:
    0. понедельник
    1. вторник
    2. среда
    3. четверг
    4. пятница
    5. суббота
    6. воскресенье
    Ваш выбор: """)
        temp = i.split()
        d = [str(n) for n in parse_days(i)]
        if len(temp) != len(d):
            print('Предупреждение: повторяющиеся номера игнорируются.')
    except ValueError:
        print("Неверный ввод.", file=sys.stderr)
        sys.exit(1)
    return i


def input_period():
    """Returns a valid relativedate object."""
    choice_period = input("""1. выбрать промежуток в днях
    2. выбрать промежуток в месяцах
    3. выбрать промежуток в годах""")
    try:
        amount = int(input("Количество: "))
    except ValueError:
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered an invalid interval - '{}'".format(amount), exc_info=True)
        sys.exit(1)

    if choice_period == '1':
        return relativedelta(days=amount)
    elif choice_period == '2':
        return relativedelta(months=amount)
    elif choice_period == '3':
        return relativedelta(years=amount)
    else:
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered an invalid number - '{}'".format(choice_period))
        sys.exit(1)


def check_user_sure(msg):
    """Asks user msg and exit if he answers no."""
    choice = input(msg + " (y/n): ")
    if choice.lower() == 'n':
        print("Команда отменена.")
        sys.exit(0)
    elif choice.lower() != 'y':
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered '{}' instead of 'y' or 'n'".format(choice))
        sys.exit(1)


def validate_status(ctx, param, value):
    """Returns Status."""
    if not value:
        return Status.ACTIVE
    if value:
        try:
            status = str_to_status(value)
        except ValueError as msg:
            print(msg)
            logger.error("Entered an invalid access level - '{}'".format(value), exc_info=True)
            sys.exit(1)
        return status


def validate_login(ctx, param, value):
    """If not login, return login of current user."""
    if not value:
        login = config_parser.get_current_user_login()
        return login
    return value


def validate_date(ctx, param, value):
    """Return datetime.date object."""
    try:
        date = to_date(value)
    except ValueError as err:
        print(err)
        logger.info("Entered an invalid date format - '{}'".format(value))
        sys.exit(1)
    return date
