"""
This module contains the name of the configuration and log files, the paths to the database,
the configuration file and the log file, also contains logging format
"""
import os

LOG_FILE_NAME = 'log_info.log'
CONFIG_FILE_NAME = 'config.ini'
APPLICATION_DATA_PATH = os.path.join(os.environ['HOME'], 'track data')
DATABASE_PATH = os.path.join(APPLICATION_DATA_PATH, 'track')
LOG_FILE_PATH = os.path.join(APPLICATION_DATA_PATH, LOG_FILE_NAME)
CONFIG_FILE_PATH = os.path.join(APPLICATION_DATA_PATH, CONFIG_FILE_NAME)
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


try:
    if not os.path.exists(APPLICATION_DATA_PATH):
        os.mkdir(APPLICATION_DATA_PATH)
except:
    print("Ошибка: не удается создать директорию: {}".format(APPLICATION_DATA_PATH))
    exit(1)
