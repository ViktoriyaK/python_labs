"""
This module contains functions for tracker application.

Functions of this module allow to perform various operations with tasks/subtasks, events, events plans,
users and groups.
"""
import click
import logging
import sys
from track_lib.tools import *
from track_lib.storage import (
    TaskStorage,
    EventStorage,
    PlanStorage,
    UserStorage,
    GroupStorage
)
from track_lib.user import User
from track_lib.task import Subtask
from track_lib.event import Event
from track_lib.plan import EventPlan, EventPlanDays
from console_lib import console_tools
from console_lib import config_parser
from console_lib import config


logger = logging.getLogger(__name__)


def add_task():
    """Add task to current user.

    All information about the new task entered by the user.
    In the case of incorrect input application ends with code 1.
    """
    click.secho('Новая задача', bg='blue', fg='white')
    task = console_tools.get_task()
    user_id = config_parser.get_current_user_id()
    TaskStorage.save_task(task, user_id)
    logger.info("Task added  id='{}'".format(task.id))
    click.secho("Задача '{}' успешно добавлена.".format(task.title), bold=True)


def add_event():
    """Add event to current user.

    All information about the new event entered by the user.
    In the case of incorrect input application ends with code 1.
    """
    click.secho('Новое мероприятие', bg='blue', fg='white')
    event = console_tools.get_event()
    user_id = config_parser.get_current_user_id()
    EventStorage.save_event(event, user_id)
    click.secho("Мероприятие успешно добавлено.", bold=True)
    logger.info("Event added.  id='{}'".format(event.id))


def add_subtask(title):
    """Add subtask to task by title to current user.

    If the task with this title is not found, the function exits.
    In the case that more than one task with this title found, displayed
    all information about them and asked to select one.
    All information about the new subtask entered by the user.
    In the case of incorrect input application ends with code 1.

    :param title: the title of the parent task
    """
    user_id = config_parser.get_current_user_id()
    tasks = TaskStorage.get_user_tasks_by_title(title, user_id)
    i = console_tools.get_task_index_from_user(tasks, title)
    task = tasks[i]
    new_subtask = console_tools.get_task()
    subtask = Subtask.create_from_task(task, new_subtask)
    TaskStorage.save_task(subtask, user_id, task.id)
    click.secho("Подзадача успешно добавлена.", bold=True)
    logger.info("Subtask added.  id='{}'".format(subtask.id))


def add_user():
    """Add new user.

    This function checks the uniqueness of the new user's login
    and asks to enter a new one until it is unique
    """
    users = get_users_()
    first_name = input("Имя: ")
    last_name = input("Фамилия: ")
    login = input("Login: ")
    while login in users:
        print("Пользователь с таким логином уже существует.")
        login = input("Login: ")
    email = input("email: ")
    new_user = User(str(first_name + " " + last_name), login, email)
    UserStorage.save_user(new_user)
    logger.info("User added.  id='{}'".format(new_user.id))
    return new_user


def get_users_():
    """Get all users.

    Returns:
            Dictionary in which login is key and user id is value
    """
    return UserStorage.get_users()


def log_in(login):
    """Logs in user with this login.

    The login and user id are written to the app config file.
    If the user with this login is not found, the function exits.
    """
    users = get_users_()
    if login in users:
        config_parser.set_current_user(login, users[login])
        click.echo("Вы зашли как " + click.style(login, bold=True) + ".")
        logger.info("User '{}' log in.  id='{}'".format(login, str(users[login])))
    else:
        print("Пользователь с логином {} не найден. Используйте команду 'add' с аргументом 'user', чтобы добавить "
              "нового пользователя.".format(login))


def log_out():
    """Logs out current user.

    This function delete login and user id from app config file.
    """
    login = config_parser.get_current_user_login()
    user_id = config_parser.get_current_user_id()
    config_parser.log_out()
    logger.info("User '{}' log out.  id='{}'".format(login, user_id))


def done_(title):
    """Mark task ready.

    If the task with this title is not found, the function exits.
    In the case that more than one task with this title found, displayed
    all information about them and asked to select one.
    If task has subtasks, all of them are marked ready.
    Parents tasks of this task also marked ready if all them subtasks are ready.
    """
    user_id = config_parser.get_current_user_id()
    tasks = TaskStorage.get_user_tasks_by_title(title, user_id)
    i = console_tools.get_task_index_from_user(tasks, title)
    _recursive_done_down(tasks[i])
    _recursive_done_up(tasks[i], user_id)


def _recursive_done_down(task):
    """Mark all subtasks ready"""
    _done_task(task)
    for subtask in task.subtasks:
        _recursive_done_down(subtask)


def _recursive_done_up(task, user_id):
    """Mark parent task ready if all subtasks are ready"""
    while isinstance(task, Subtask):
        task = TaskStorage.get_user_task_by_id(task.parent_task.id, user_id)
        is_ready = True
        for sub in task.subtasks:
            if sub.status != Status.READY:
                is_ready = False
        if is_ready:
            _done_task(task)


def _done_task(task):
    task.status = Status.READY
    TaskStorage.update_task(task)
    print("Задача '{}' выполнена.".format(task.title))
    logger.info("Task completed.  id='{}'".format(task.id))


def show_day(status, date_=datetime.now().date()):
    """Display all tasks, events and events plans of the current user on this date

    Args:
        status (Status): Status of the displayed tasks.
        date_ (datetime.date): Date of events and tasks.
    """
    user_id = config_parser.get_current_user_id()

    events = EventStorage.get_user_events_by_date(date_, user_id)
    tasks = TaskStorage.get_user_tasks_by_date(date_, user_id, status)
    tasks_without_deadline = TaskStorage.get_user_tasks_by_date(None, user_id, status)

    event_plans = PlanStorage.get_user_event_plans(user_id)
    events.extend([p.event for p in event_plans if p.is_today(date_)])
    events = sorted(events, key=Event.sort_by_time)

    if not (events or tasks):
        click.secho("На {} ничего не найдено.".format(date_to_str(date_)), bold=True)
    console_tools.display_lists(events, tasks, tasks_without_deadline)


def show_groups():
    """Display all groups."""
    user_id = config_parser.get_current_user_id()
    groups = sorted(GroupStorage.get_groups(user_id))
    if groups:
        click.secho("Группы:", bold=True, bg="white", fg="black")
        for group in groups:
            print("* {}".format(group))


def show_group(name, status):
    """Display tasks, events and events plans of this group."""
    user_id = config_parser.get_current_user_id()
    events = sorted(EventStorage.get_user_events_by_group(name, user_id), key=Event.sort_by_time)
    tasks = TaskStorage.get_user_tasks_by_group(name, user_id, status)
    tasks_without_deadline = TaskStorage.get_user_tasks_by_group(name, user_id, status, is_without_deadline=True)
    if not (events or tasks or tasks_without_deadline):
        print("Ничего для группы '{}' не найдено.".format(name))
        return
    console_tools.display_lists(events, tasks, tasks_without_deadline, True)


def get_user_task(title, status, login):
    """Display all tasks with this title and status of the user with this login.

    If the login is not login of the current user, before the display, access is checked.

    Args:
        title (str): The title of the displayed tasks.
        status (Status): The status of the displayed tasks.
        login (str): The login of the user.
    """
    current_user_login = config_parser.get_current_user_login()
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)

    user_id = users[login]
    tasks = TaskStorage.get_user_tasks_by_title(title, user_id, status)
    if not tasks:
        print("Задача с заголовком '{}' не найдена.".format(title))
        sys.exit(0)
    if login != current_user_login:
        is_private = True
        for task in tasks:
            if task.access == Access.READE or task.access == Access.WRITE:
                console_tools.display_task_full(task, show_status=True, show_create_date=True, check_access=True,
                                                show_edit_date=True)
                print()
                is_private = False
        if is_private:
            print("Доступ к просмотру закрыт.")
    else:
        for task in tasks:
            console_tools.display_task_full(task, show_status=True, show_create_date=True, show_edit_date=True)
            print()


def get_user_event(title, login):
    """Display all events with this title of the user with this login.

    If the login is not login of the current user, before the display, access is checked.

    Args:
        title (str): The title of the displayed events.
        login (str): The login of the user.
    """
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)
    current_user_login = config_parser.get_current_user_login()
    user_id = users[login]
    events = EventStorage.get_user_events_by_title(title, user_id)
    if not events:
        print("Мероприятие с заголовком '{}' не найдено.".format(title))
        sys.exit(0)
    if login != current_user_login:
        is_private = True
        for event in events:
            if event.access == Access.READE or event.access == Access.WRITE:
                console_tools.display_event_full(event, show_create_date=True, show_edit_date=True)
                is_private = False
                print()
        if is_private:
            print("Доступ к просмотру закрыт.")
    else:
        for event in events:
            console_tools.display_event_full(event, show_create_date=True, show_edit_date=True)
            print()


def get_user_event_plan(title, login):
    """Display all event plans with this title of the user with this login.

    If the login is not login of the current user, before the display, access is checked.

    Args:
        title (str): The title of the event plan.
        login (str): The login of the user.
    """
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)
    current_user_login = config_parser.get_current_user_login()

    user_id = users[login]
    plans = PlanStorage.get_user_event_plans_by_title(title, user_id)
    if not plans:
        print("Мероприятие с заголовком '{}' не найдено.".format(title))
        sys.exit(0)
    if login != current_user_login:
        is_private = True
        for plan in plans:
            if plan.event.access == Access.READE or plan.event.access == Access.WRITE:
                console_tools.display_plan(plan)
                is_private = False
                print()
        if is_private:
            print("Доступ к просмотру закрыт.")
    else:
        for plan in plans:
            console_tools.display_plan(plan)
            print()


def edit_task(title, status, login):
    """Edit task with this title and status of the user with this login.

    If the task with this title is not found, the function exits.
    In the case that more than one task with this title found, displayed
    all information about them and asked to select one. If the login is not login
    of the current user, before the display, access is checked.

    Args:
        title (str): The title of the task.
        status (Status): The status of the task.
        login (str): The login of the user.
    """
    current_user_id = config_parser.get_current_user_id()
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)

    user_id = users[login]

    tasks = TaskStorage.get_user_tasks_by_title(title, user_id, status)
    if not tasks:
        print("Задача с заголовком '{}' не найдена.".format(title))
        sys.exit(0)
    if str(current_user_id) != str(user_id):
        tasks = [task for task in tasks if task.access == Access.WRITE]
    if not tasks:
        print("Доступ закрыт.")
        sys.exit(0)
    i = console_tools.get_task_index_from_user(tasks, title)
    _edit_task_field(tasks[i])


def _edit_task_field(task):
    """Edit task field which is selected by user.

    New values are entered by user.
    In the case of incorrect input application ends with code 1.
    """
    print()
    console_tools.display_task_full(task, show_subtasks=False)
    print()
    choice = input("Вы хотите изменить: ")

    if choice == '1':
        task.title = console_tools.input_title()
        TaskStorage.update_task(task)

    elif choice == '2':
        task.deadline_date = console_tools.input_date("Новый дедлайн: ")
        TaskStorage.update_task(task)

    elif choice == '3':
        task.priority = console_tools.input_priority()
        TaskStorage.update_task(task)

    elif choice == '4':
        task.description = empty_to_none(input("Новое описание: "))
        TaskStorage.update_task(task)

    elif choice == '5':
        task.related_tasks = empty_to_none(input("Новые связные задачи: "))
        TaskStorage.update_task(task)

    elif choice == '6':
        task.group = empty_to_none(input("Новая группа: "))
        TaskStorage.update_task(task)

    elif choice == '7':
        task.access = console_tools.input_access()
        TaskStorage.update_task(task)

    else:
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered an invalid edit action number for task - '{}'".format(choice))
        exit(1)

    print("Изменения сохранены.")


def edit_event(title, login):
    """Edit event by title of the user with this login.

    If the event with this title is not found, the function exits.
    In the case that more than one event with this title found, displayed
    all information about them and asked to select one. If the login is not login
    of the current user, before the display, access is checked.

    Args:
        title (str): The title of the event.
        login (str): The login of the user.
    """
    current_user_id = config_parser.get_current_user_id()
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)

    user_id = users[login]

    events = EventStorage.get_user_events_by_title(title, user_id)
    if not events:
        print("Мероприятие с заголовком '{}' не найдено.".format(title))
        sys.exit(0)
    if str(current_user_id) != str(user_id):
        events = [event for event in events if event.access == Access.WRITE]
    if not events:
        print("Доступ закрыт.")
        sys.exit(0)
    i = console_tools.get_event_index_from_user(events, title)
    _edit_event_field(events[i])


def _edit_event_field(event):
    """Edit event field which is selected by user.

    New values are entered by user.
    In the case of incorrect input application ends with code 1.
    """
    print()
    console_tools.display_event_full(event)
    print()
    choice = input("Вы хотите изменить: ")

    if choice == '1':
        event.title = console_tools.input_title()
        EventStorage.update_event(event)

    elif choice == '2':
        event.date = console_tools.input_date("Новая дата: ")
        EventStorage.update_event(event)

    elif choice == '3':
        event.time = console_tools.input_time()
        EventStorage.update_event(event)

    elif choice == '4':
        event.priority = console_tools.input_priority()
        EventStorage.update_event(event)

    elif choice == '5':
        event.group = empty_to_none(input("Новая группа: "))
        EventStorage.update_event(event)

    elif choice == '6':
        event.description = empty_to_none(input("Новое описание: "))
        EventStorage.update_event(event)

    elif choice == '7':
        event.access = console_tools.input_access()
        EventStorage.update_event(event)
    else:
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered an invalid edit action number for event - '{}'".format(choice))
        sys.exit(1)

    print("Изменения сохранены.")


def add_event_plan():
    """Add event plan to the current user.

    All information about the new event plan entered by the user.
    In the case of incorrect input application ends with code 1.
    """
    user_id = config_parser.get_current_user_id()

    click.secho('Новый план мероприятий', bg='green', fg='white', bold=True)
    event = console_tools.get_event()
    event.is_planned = True
    choice = input("""Повторять:
1. каждый день
2. выбрать дни
3. выбрать промежуток в днях
4. выбрать промежуток в месяцах
5. выбрать промежуток в годах
Ваш выбор: """)

    if choice == '1':
        event_plan = EventPlan(event, relativedelta(days=1), event.date)
        event.plan_id = event_plan.id
        event.id = EventStorage.save_event(event, user_id)
        PlanStorage.save_event_plan(event_plan, user_id)

    elif choice == '2':
        days = console_tools.input_week_days()
        event_plan_day = EventPlanDays(event, days, event.date)
        event.plan_id = event_plan_day.id
        event.id = EventStorage.save_event(event, user_id)
        PlanStorage.save_event_plan_days(event_plan_day, user_id)

    elif choice == '3':
        try:
            days = input("Количество дней: ")
            days = int(days)
        except ValueError:
            print("Неверный ввод.", file=sys.stderr)
            logger.error("Entered an invalid days amount - '{}'".format(days), exc_info=True)
            sys.exit(1)
        event_plan = EventPlan(event, relativedelta(days=days), event.date)
        event.plan_id = event_plan.id
        event.id = EventStorage.save_event(event, user_id)
        PlanStorage.save_event_plan(event_plan, user_id)

    elif choice == '4':
        try:
            month = input("Количество месяцов: ")
            month = int(month)
        except ValueError:
            print("Неверный ввод.", file=sys.stderr)
            logger.error("Entered an invalid months amount - '{}'".format(month), exc_info=True)
            sys.exit(1)
        event_plan = EventPlan(event, relativedelta(months=month), event.date)
        event.plan_id = event_plan.id
        event.id = EventStorage.save_event(event, user_id)
        PlanStorage.save_event_plan(event_plan, user_id)

    elif choice == '5':
        try:
            years = input("Количество лет: ")
            years = int(years)
        except ValueError:
            print("Неверный ввод.", file=sys.stderr)
            logger.error("Entered an invalid months amount - '{}'".format(years), exc_info=True)
            sys.exit(1)
        event_plan = EventPlan(event, relativedelta(years=years), event.date)
        event.plan_id = event_plan.id
        event.id = EventStorage.save_event(event, user_id)
        PlanStorage.save_event_plan(event_plan, user_id)

    else:
        print("Неверный ввод.", file=sys.stderr)
        logger.error("Entered an invalid edit action number for plan event - '{}'".format(choice))
        sys.exit(1)

    print("План мероприятия успешно сохранен.")


def edit_event_plan(title, login):
    """Edit event plan of the user with this login.

    If the event plan with this title is not found, the function exits.
    In the case that more than one event plan with this title found, displayed
    all information about them and asked to select one. If the login is not login
    of the current user, before the display, access is checked.

    Args:
        title (str): The title of the event.
        login (str): The login of the user.
    """
    current_user_id = config_parser.get_current_user_id()
    users = get_users_()
    if not (login in users):
        print("Пользователь с логином '{}' не найден.".format(login))
        sys.exit(0)

    user_id = users[login]

    plans = PlanStorage.get_user_event_plans_by_title(title, user_id)
    if not plans:
        print("Планового мероприятия с заголовком '{}' не найдено.".format(title))
        exit(0)
    if str(current_user_id) != str(user_id):
        plans = [plan for plan in plans if plan.event.access == Access.WRITE]
    if not plans:
        print("Доступ закрыт.")
        exit(0)
    i = console_tools.get_event_plan_index_from_user(plans, title)
    _edit_plan_field(plans[i])


def _edit_plan_field(plan):
    """Edit event plan field which is selected by user.

    New values are entered by user.
    In the case of incorrect input application ends with code 1.
    """
    click.secho('План', bg='green', fg='white', bold=True)
    click.echo(click.style('1. Начальная дата: ', bold=True) + date_to_str(plan.first_date))

    if isinstance(plan, EventPlan):

        click.echo(click.style('2. Промежуток повторения: ', bold=True) + rel_to_display(plan.delta_date))
        print()
        choice = input("Вы хотите изменить: ")

        if choice == '1':
            plan.first_date = console_tools.input_date("Новая дата: ")
            PlanStorage.update_event_plan(plan)

        elif choice == '2':
            plan.delta_date = console_tools.input_period()
            PlanStorage.update_event_plan(plan)

        else:
            print("Неверный ввод.", file=sys.stderr)
            logger.error("Entered an invalid edit action number for plan - '{}'".format(choice))
            sys.exit(1)

    else:
        click.echo(click.style('2. Дни повторений: ', bold=True) + plan_days_to_display(plan.days))
        print()
        choice = input("Вы хотите изменить: ")

        if choice == '1':
            plan.first_date = console_tools.input_date("Новая дата: ")
            PlanStorage.update_event_plan_days(plan)

        elif choice == '2':
            plan.days = console_tools.input_week_days()
            PlanStorage.update_event_plan_days(plan)

        else:
            print("Неверный ввод.", file=sys.stderr)
            logger.error("Entered an invalid edit action number for plan - '{}'".format(choice))
            sys.exit(1)

    print("Изменения сохранены.")


def display_failed(failed):
    """Displays titles of failed tasks."""
    for task in failed:
        print("Задача '{}' провалена!".format(task))


def set_logger():
    """Sets logging configuration."""
    logging.basicConfig(filename=config.LOG_FILE_PATH, level=logging.INFO, format=config.LOG_FORMAT)


def delete_task(title, status):
    """Deletes task of the current user by title.

    If the task with this title is not found, the function exits.
    In the case that more than one task with this title found, displayed
    all information about them and asked to select one.
    """
    user_id = config_parser.get_current_user_id()

    tasks = TaskStorage.get_user_tasks_by_title(title, user_id, status=status)
    i = console_tools.get_task_index_from_user(tasks, title)
    console_tools.check_user_sure("Задача и все ее подзадачи (если они есть) будут удалены. Вы уверены, "
                                  "что хотите прододжить?")
    TaskStorage.delete_task(tasks[i].id)

    print("Задача '{}' удалена.".format(tasks[i].title))
    logger.info("Task deleted.  id='{}'".format(tasks[i].id))


def delete_event(title):
    """Deletes event of the current user by title.

    If the event with this title is not found, the function exits.
    In the case that more than one event with this title found, displayed
    all information about them and asked to select one.
    """
    user_id = config_parser.get_current_user_id()

    events = EventStorage.get_user_events_by_title(title, user_id)
    i = console_tools.get_event_index_from_user(events, title)
    console_tools.check_user_sure("Мероприятие будет удалено. Вы уверены, что хотите продолжить?")
    EventStorage.delete_event(events[i].id)

    print("Мероприятие '{}' удалено.".format(events[i].title))
    logger.info("Event deleted.  id='{}'".format(events[i].id))


def delete_event_plan(title):
    """Deletes event plan of the current user by title.

    If the event plan with this title is not found, the function exits.
    In the case that more than one event plan with this title found, displayed
    all information about them and asked to select one.
    """
    user_id = config_parser.get_current_user_id()

    plans = PlanStorage.get_user_event_plans_by_title(title, user_id)
    i = console_tools.get_event_plan_index_from_user(plans, title)
    console_tools.check_user_sure("Плановое мероприятие будет удалено. Вы уверены, что хотите продолжить?")
    EventStorage.delete_event(plans[i].event.id)

    print("Плановое мероприятие '{}' удалено.".format(plans[i].event.title))
    logger.info("Plan event deleted.  id='{}'".format(plans[i].event.id))


def delete_user():
    """Deletes current user and all his information (tasks, events, event plans)."""
    user_id = config_parser.get_current_user_id()

    login = config_parser.get_current_user_login()

    console_tools.check_user_sure("Пользователь '{}' и все его данный (в том числе задачи, мероприятия и т.д.)"
                                  "будут удалены. Вы уверены, что хотите продолжить?".format(login))
    UserStorage.delete_user(user_id)
    config_parser.log_out()

    print("Аккаунт пользователя '{}' удален.".format(login))
    logger.info("User account deleted.  id='{}'".format(user_id))


def change_user(login):
    """Changes current user.

    Args:
        login (str): The login of new current user."""
    user_login = config_parser.get_current_user_login()
    users = get_users_()
    if login == user_login:
        print("Вы уже зашли от аккаунта этого пользователя.")
    elif login in users:
        config_parser.set_current_user(login, users[login])
        click.echo("Вы зашли как " + click.style(login, bold=True) + ".")
        logger.info("User '{}' log out.  id='{}'".format(user_login, str(users[user_login])))
        logger.info("User '{}' log in.  id='{}'".format(login, str(users[login])))
    else:
        print("Пользователь с логином {} не найден. Используйте команду 'add' с аргументом 'user', чтобы добавить "
              "нового пользователя.".format(login))
