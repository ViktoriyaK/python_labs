"""Tracker application entry point"""

import click
from console_lib import console
from console_lib import console_tools
from console_lib import config_parser
from console_lib import config
from track_lib.db import Adapter
from track_lib.storage import TaskStorage


@click.group()
def cli(invoke_without_command=True):
    adapter = Adapter(config.DATABASE_PATH)
    console.display_failed(TaskStorage.update_status())
    console.set_logger()


@cli.group(help='Команда для работы с задачами')
def task():
    pass


@task.command(help='Добавить задачу')
@click.option('--parent', '-p', help='Заголовок задачи, к которой хотите добавить подзадачу')
def add(parent):
    if parent:
        console.add_subtask(parent)
    else:
        console.add_task()


@task.command(help='Показать задачу пользователя')
@click.argument('title')
@click.option('--status', callback=console_tools.validate_status, type=str,
              help='Статус задачи (active, ready, failed)')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def show(title, status, login):
    console.get_user_task(title, status, login)


@task.command(help='Редактировать задачу пользователя')
@click.argument('title')
@click.option('--status', callback=console_tools.validate_status, type=str,
              help='Статус задачи (active, ready, failed)')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def edit(title, status, login):
    console.edit_task(title, status, login)


@task.command(help='Отметить задачу выполненной')
@click.argument('title')
def done(title):
    console.done_(title)


@task.command(help='Удалить задачу')
@click.argument('title')
@click.option('--status', callback=console_tools.validate_status, type=str,
              help='Статус задачи (active, ready, failed)')
def delete(title, status):
    console.delete_task(title, status)


@cli.group(help='Команда для работы с мероприятиями')
def event():
    pass


@event.command(help='Добавить мероприятие')
def add():
    console.add_event()


@event.command(help='Показать мероприятие')
@click.argument('title')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def show(title, login):
    console.get_user_event(title, login)


@event.command(help='Редактировать мероприятие пользователя')
@click.argument('title')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def edit(title, login):
    console.edit_event(title, login)


@event.command(help='Удалить мероприятие')
@click.argument('title')
def delete(title):
    console.delete_event(title)


@cli.group(help='Команда для работы с планами мероприятий')
def plan():
    pass


@plan.command(help='Добавить план мероприятия')
def add():
    console.add_event_plan()


@plan.command(help='Показать план мероприятия')
@click.argument('title')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def show(title, login):
    console.get_user_event_plan(title, login)


@plan.command(help='Редактировать план мероприятия пользователя')
@click.argument('title')
@click.option('--login', callback=console_tools.validate_login, type=str, help='Логин пользователя')
def edit(title, login):
    console.edit_event_plan(title, login)


@plan.command(help='Удалить план мероприятия')
@click.argument('title')
def delete(title):
    console.delete_event_plan(title)


@cli.group(help='Команда для работы с пользователем')
def user():
    pass


@user.command(help='Добавить пользователя')
def add():
    new_user = console.add_user()
    choice = input("Пользователь добавлен. Хотите зайти с нового аккаунта? (y/n): ")
    if choice.lower() == 'y' or choice.lower == 'д':
        new_id = console.get_users_()[new_user.login]
        config_parser.set_current_user(new_user.login, new_id)


@user.command(help='Сменить пользователя')
@click.argument('login')
def change(login):
    console.change_user(login)


@user.command(help='Показать текущего пользователя')
def current():
    user_login = config_parser.get_current_user_login()
    click.echo("Вы зашли как " + click.style(user_login, bold=True) + ".")


@user.command(help='Удалить аккаунт текущего пользователя')
def delete():
    console.delete_user()


@user.command(help='Выполнить вход в аккаунт')
@click.argument('login')
def login(login):
    console.log_in(login)


@user.command(help='Выполнить выход из аккаунта')
def logout():
    console.log_out()


@cli.command(help='Показать логины всех пользователей')
def users():
    users = console.get_users_()
    if not users:
        print("Пусто!")
    else:
        click.secho("Пользователи:", bold=True, bg="white", fg="black")
        for user in users.keys():
            print("* {}".format(user))


@cli.command(help='Показать задачи и мероприятия на сегодня')
@click.option('--status', callback=console_tools.validate_status, type=str, help='Статус задач')
def today(status):
    console.show_day(status)


@cli.command(help='Показать задачи и мероприятия в этот день')
@click.argument('date', callback=console_tools.validate_date)
@click.option('--status', callback=console_tools.validate_status, type=str, help='Статус задач')
def day(date, status):
    console.show_day(status, date)


@cli.command(help='Показать содержимое группы')
@click.argument('name')
@click.option('--status', callback=console_tools.validate_status, type=str, help='Статус задач')
def group(name, status):
    console.show_group(name, status)


@cli.command(help='Показать все группы')
def groups():
    console.show_groups()
