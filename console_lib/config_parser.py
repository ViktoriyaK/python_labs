"""
This module contains functions that read and set current user info in config file
"""
import configparser
import sys
from console_lib import config as con

NO_USER_SECTION_MSG = ("Вы не выполнили вход. Используйте команду 'login', чтобы войти в аккаунт или команду 'add' с "
                       "аргументом 'user', чтобы создать новый аккаунт. ")


def _read_section(section_name, option_name):
    config = configparser.ConfigParser()
    config.read(con.CONFIG_FILE_PATH)
    return config.get(section_name, option_name)


def _write_section(section_name, option_name, value):
    config = configparser.ConfigParser()
    config.read(con.CONFIG_FILE_PATH)
    config.set(section_name, option_name, value)
    with open(con.CONFIG_FILE_PATH, "w") as config_file:
        config.write(config_file)


def get_current_user_id():
    """Return id of the current user"""
    try:
        return _read_section("User", "user_id")
    except configparser.NoOptionError:
        print(NO_USER_SECTION_MSG)
        sys.exit(1)


def get_current_user_login():
    """Return login of the current user"""
    try:
        return _read_section("User", "login")
    except configparser.NoOptionError:
        print(NO_USER_SECTION_MSG)
        sys.exit(1)


def set_current_user(login, user_id):
    """Write login and id of the current user into config file"""
    _write_section("User", "login", login)
    _write_section("User", "user_id", str(user_id))


def log_out():
    """Remove current user info from config file"""
    config = configparser.ConfigParser()
    try:
        config.read(con.CONFIG_FILE_PATH)
        config.remove_option("User", "user_id")
        config.remove_option("User", "login")
        with open(con.CONFIG_FILE_PATH, "w") as config_file:
            config.write(config_file)
        print("Выход выполнен.")
    except configparser.NoSectionError:
        print("Вы еще не выполнили вход.")
